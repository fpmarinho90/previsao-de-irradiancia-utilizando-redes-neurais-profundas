# Previsão de irradiância utilizando redes neurais profundas

Neste projeto, foi realizado a previsão de irradiância solar nos horizontes de 5, 10, 15, 20, 25 e 30 minutos a posteriori pela aplicação de Redes Neurais Profunda (CNN-1D, LSTM e CNN-LSTM) no conjunto Folsom.DATA 